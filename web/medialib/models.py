from typing import Generator

from django.db import models

from medialib.tasks import update_content_item_counter


class Page(models.Model):

    title = models.CharField(max_length=255, null=False, blank=False)

    @property
    def content(self):
        return list(ContentOnPage.objects.get_content_for_page(self))

    def increase_counter_on_related_content(self):
        for content_item in self.content:
            update_content_item_counter.delay(type(content_item).__name__, content_item.pk)

    def __str__(self):
        return f'Page: {self.title}'

    class Meta:
        ordering = ('id', )


class Content(models.Model):

    title = models.CharField(max_length=255, null=False, blank=False)
    counter = models.PositiveIntegerField(null=False, blank=False, default=0)
    page = models.ManyToManyField('Page', related_name='%(class)ss', through='ContentOnPage')

    class Meta:
        abstract = True

    def __str__(self):
        return f'{type(self).__name__}: {self.title}'


class Video(Content):

    video_url = models.URLField(max_length=255, null=False, blank=False)
    subtitles_url = models.URLField(max_length=255, null=True, blank=True)


class Audio(Content):

    bitrate = models.PositiveIntegerField(null=False, blank=False)


class Text(Content):

    text = models.TextField()


class ContentOnPageModelManager(models.Manager):

    def create(self, **kwargs):
        if 'order_nbr' not in kwargs:
            try:
                order_nbr = ContentOnPage.objects.filter(page=kwargs['page']).latest('order_nbr').order_nbr
            except ContentOnPage.DoesNotExist:
                order_nbr = 0
            # TODO (dmitry): race condition (use select_for_update?)
            kwargs['order_nbr'] = order_nbr + 1
        return super().create(**kwargs)

    def get_content_for_page(self, page: Page) -> Generator[Content, None, None]:
        """
        Returns a list of content for given page. Content is ordered by `order_nbr`
        field (which is the marker for a content position on the page).

        :param page: given page to get content for
        :return: list of content on givent page
        """
        for content_item in ContentOnPage.objects.filter(page=page).order_by('order_nbr'):
            if content_item.video:
                yield content_item.video
                continue
            if content_item.audio:
                yield content_item.audio
                continue
            if content_item.text:
                yield content_item.text
                continue

    def swap_content_items(self, content_item_source: Content, content_item_destination: Content):
        content_item_source.order_nbr, content_item_destination.order_nbr = \
            content_item_destination.order_nbr, content_item_source.order_nbr
        content_item_source.save()
        content_item_destination.save()


class ContentOnPage(models.Model):

    objects = ContentOnPageModelManager()

    page = models.ForeignKey('Page', on_delete=models.CASCADE)
    video = models.ForeignKey('Video', null=True, on_delete=models.CASCADE)
    audio = models.ForeignKey('Audio', null=True, on_delete=models.CASCADE)
    text = models.ForeignKey('Text', null=True, on_delete=models.CASCADE)
    order_nbr = models.PositiveIntegerField(default=0)

    # TODO (dmitry): add integrity check (to ensure strictly 1 content to be set in a row)
