from celery import shared_task

from django.apps import apps
from django.db.models import F


@shared_task
def update_content_item_counter(content_item_cls: str, content_item_pk: int):
    # TODO (dmitry): remove coupling with app name
    content_item_cls = apps.get_model('medialib', content_item_cls)
    content_item_cls.objects.filter(pk=content_item_pk).update(
        counter=F('counter') + 1
    )
