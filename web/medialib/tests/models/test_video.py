from django.db import models
from django.test import TestCase

from medialib.models import Content, Video


class VideoModelTestCase(TestCase):

    def test_video_model_inherited_from_content_base_class(self):
        self.assertTrue(issubclass(Video, Content))

    def test_video_has_video_url(self):
        video_url = Video._meta.get_field('video_url')
        self.assertIsInstance(video_url, models.URLField)

    def test_video_url_field_attributes(self):
        video_url = Video._meta.get_field('video_url')

        self.assertEqual(video_url.max_length, 255)
        self.assertFalse(video_url.null)
        self.assertFalse(video_url.blank)

    def test_video_has_subtitles_url(self):
        subtitles_url = Video._meta.get_field('subtitles_url')
        self.assertIsInstance(subtitles_url, models.URLField)

    def test_subtitles_url_field_arguments(self):
        subtitles_url = Video._meta.get_field('subtitles_url')

        self.assertEqual(subtitles_url.max_length, 255)
        self.assertTrue(subtitles_url.null)
        self.assertTrue(subtitles_url.blank)
