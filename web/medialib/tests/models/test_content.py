from django.db import models
from django.test import TestCase

from medialib.models import Content


class ContentModelTestCase(TestCase):

    def test_content_model_is_abstract(self):
        self.assertTrue(Content._meta.abstract)

    def test_content_has_title(self):
        title = Content._meta.get_field('title')

        self.assertIsInstance(title, models.CharField)

    def test_title_field_arguments(self):
        title = Content._meta.get_field('title')

        self.assertEqual(title.max_length, 255)
        self.assertFalse(title.null)
        self.assertFalse(title.blank)

    def test_content_has_counter(self):
        counter = Content._meta.get_field('counter')

        self.assertIsInstance(counter, models.PositiveIntegerField)

    def test_counter_field_arguments(self):
        counter = Content._meta.get_field('counter')

        self.assertFalse(counter.null)
        self.assertFalse(counter.blank)

        self.assertEqual(counter.default, 0)
