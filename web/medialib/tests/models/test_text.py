from django.db import models
from django.test import TestCase

from medialib.models import Content, Text


class VideoModelTestCase(TestCase):

    def test_text_model_inherited_from_content_base_class(self):
        self.assertTrue(issubclass(Text, Content))

    def test_text_has_text(self):
        text = Text._meta.get_field('text')

        self.assertIsInstance(text, models.TextField)

    def test_text_field_arguments(self):
        text = Text._meta.get_field('text')

        self.assertFalse(text.null)
        self.assertFalse(text.blank)
