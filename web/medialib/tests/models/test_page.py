from django.db import models
from django.test import TestCase

from medialib.models import Page


class PageModelStructureTestCase(TestCase):

    def test_page_has_title(self):
        title = Page._meta.get_field('title')

        self.assertIsInstance(title, models.CharField)

    def test_title_field_arguments(self):
        title = Page._meta.get_field('title')

        self.assertEqual(title.max_length, 255)
        self.assertFalse(title.null)
        self.assertFalse(title.blank)

