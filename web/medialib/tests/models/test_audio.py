from django.db import models
from django.test import TestCase

from medialib.models import Content, Audio


class VideoModelTestCase(TestCase):

    def test_audio_model_inherited_from_content_base_class(self):
        self.assertTrue(issubclass(Audio, Content))

    def test_audio_has_bitrate(self):
        bitrate = Audio._meta.get_field('bitrate')
        self.assertIsInstance(bitrate, models.PositiveIntegerField)

    def test_bitrate_field_arguments(self):
        bitrate = Audio._meta.get_field('bitrate')

        self.assertFalse(bitrate.null)
        self.assertFalse(bitrate.blank)
