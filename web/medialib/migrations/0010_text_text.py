# Generated by Django 2.1.7 on 2019-02-19 17:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('medialib', '0009_auto_20190219_1712'),
    ]

    operations = [
        migrations.AddField(
            model_name='text',
            name='text',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
