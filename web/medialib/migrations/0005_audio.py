# Generated by Django 2.1.7 on 2019-02-19 16:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('medialib', '0004_video'),
    ]

    operations = [
        migrations.CreateModel(
            name='Audio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('counter', models.PositiveIntegerField()),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
