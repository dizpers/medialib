# Generated by Django 2.1.7 on 2019-02-22 07:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('medialib', '0016_sample_data'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contentonpage',
            name='audio',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='medialib.Audio'),
        ),
        migrations.AlterField(
            model_name='contentonpage',
            name='text',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='medialib.Text'),
        ),
        migrations.AlterField(
            model_name='contentonpage',
            name='video',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='medialib.Video'),
        ),
    ]
