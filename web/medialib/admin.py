from django.contrib import admin

from medialib.models import Page, Video, Audio, Text, ContentOnPage


class VideoInline(admin.TabularInline):

    model = ContentOnPage


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):

    search_fields = ('title', )
    inlines = [
        VideoInline
    ]


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):

    search_fields = ('title', )


@admin.register(Audio)
class AudioAdmin(admin.ModelAdmin):

    search_fields = ('title', )


@admin.register(Text)
class TextAdmin(admin.ModelAdmin):

    search_fields = ('title', )
