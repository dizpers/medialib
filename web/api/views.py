from rest_framework import mixins, viewsets

from api.serializers import PageDetailSerializer, PageListSerializer

from medialib.models import Page


class PageViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    queryset = Page.objects.all()

    def get_serializer_class(self):
        return self.detail and PageDetailSerializer or \
                not self.detail and PageListSerializer

    def retrieve(self, request, *args, **kwargs):
        page = self.get_object()
        page.increase_counter_on_related_content()
        return super().retrieve(request, *args, **kwargs)
