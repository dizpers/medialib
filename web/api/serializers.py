from typing import Type

from rest_framework import serializers

from medialib.models import Page, Content


class PageDetailSerializer(serializers.ModelSerializer):
    _cache_serializers = {}

    content = serializers.SerializerMethodField()

    class Meta:
        model = Page
        fields = ('title', 'content', )

    def _get_content_item_serializer(self, content_item_type: Type[Content]) -> Type[serializers.ModelSerializer]:
        """
        Return the serializer class, which is inherited from model serializer.
        That new serializer will have given content item class in `Meta`.

        :param content_item_type: content item class (Audio, Video, Text, ...)
        :return: serializer class for given content item class
        """
        if content_item_type not in self._cache_serializers:
            class ContentItemSerializer(serializers.ModelSerializer):
                class Meta:
                    model = content_item_type
                    exclude = ('id', 'page', )
            self._cache_serializers[content_item_type] = ContentItemSerializer
        return self._cache_serializers[content_item_type]

    def get_content(self, page):
        return [
            self._get_content_item_serializer(type(content_item))(content_item).data for content_item in page.content
        ]


class PageListSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Page
        fields = ('url', )
        extra_kwargs = {
            'url': {'view_name': 'page-detail'}
        }
