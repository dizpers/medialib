from urllib.parse import urljoin

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from medialib.models import Page, Video, Text, ContentOnPage


class PageAPITestCase(APITestCase):

    def setUp(self):
        self.page1 = Page.objects.create(title='My Videos')

        self.page2 = Page.objects.create(title='My Videos and Text Posts')
        self.page2video1 = Video.objects.create(
            title='Martin Fowler @ OOP2014 "Workflows of Refactoring"',
            video_url='https://www.youtube.com/watch?v=vqEg37e4Mkw'
        )
        self.page2video2 = Video.objects.create(
            title='Sonic Youth - The Diamond Sea',
            video_url='https://www.youtube.com/watch?v=v_4e8Tq-CKQ',
            subtitles_url='http://example.com/sonicyouth'
        )
        self.page2text1 = Text.objects.create(
            title='My Bio',
            text='blah blah'
        )
        ContentOnPage.objects.create(page=self.page2, video=self.page2video1)
        ContentOnPage.objects.create(page=self.page2, text=self.page2text1)
        ContentOnPage.objects.create(page=self.page2, video=self.page2video2)

        self.page3 = Page.objects.create(title='My Audios and Texts')

        self.pages = [self.page1, self.page2, self.page3]

    def test_get_page_list(self):
        response = self.client.get(reverse('page-list'), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)
        self.assertListEqual(
            response.data['results'],
            [
                {
                    'url': urljoin(
                        'http://testserver/',
                        reverse('page-detail', kwargs={'pk': page.pk})
                    )
                } for page in self.pages
            ]
        )

    def test_get_page_detail(self):
        response = self.client.get(reverse('page-detail', kwargs={'pk': self.page2.pk}), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertDictEqual(
            response.data,
            {
                'title': 'My Videos and Text Posts',
                'content': [
                    {
                        'title': 'Martin Fowler @ OOP2014 "Workflows of Refactoring"',
                        'counter': 0,
                        'video_url': 'https://www.youtube.com/watch?v=vqEg37e4Mkw',
                        'subtitles_url': None
                    },
                    {
                        'title': 'My Bio',
                        'counter': 0,
                        'text': 'blah blah'
                    },
                    {
                        'title': 'Sonic Youth - The Diamond Sea',
                        'counter': 0,
                        'video_url': 'https://www.youtube.com/watch?v=v_4e8Tq-CKQ',
                        'subtitles_url': 'http://example.com/sonicyouth'
                    }
                ]
            }
        )

    def test_get_page_detail_content_order(self):
        ContentOnPage.objects.swap_content_items(
            ContentOnPage.objects.get(order_nbr=1),
            ContentOnPage.objects.get(order_nbr=3)
        )
        self.assertRaises(AssertionError, self.test_get_page_detail)
